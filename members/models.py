from django.db import models

# Create your models here.
class Members(models.Model):
    prenom = models.CharField(max_length=225)
    nom = models.CharField(max_length=225)
    email = models.EmailField(unique=True)
    age = models.IntegerField()
    bio = models.TextField(null=True, blank=True)
    residente = models.CharField(max_length=225)
    formation = models.CharField(max_length=225)
    
    def __str__ (self):
        return self.nom
